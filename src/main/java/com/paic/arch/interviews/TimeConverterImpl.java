package com.paic.arch.interviews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paic.arch.interviews.support.BehaviouralTestEmbedder;

public class TimeConverterImpl implements TimeConverter {
	
	private static final Logger LOG = LoggerFactory.getLogger(TimeConverterImpl.class);
	
	public String convertTime(String aTime) {
		
		BaseMode baseMode=new ClockModel();
		
		String[] time=aTime.split(":");
		
	   //Model数据初始化
	   if(time.length==3){
		   baseMode.setTime(aTime);
		   baseMode.setHour(time[0]);
		   baseMode.setMinute(time[1]);
		   baseMode.setSecond(time[2]);
		   baseMode.setFiveHour(Math.floorDiv(Integer.parseInt(baseMode.getHour()), 5));
		   baseMode.setFiveMinute(Math.floorDiv(Integer.parseInt(baseMode.getMinute()), 5));
		   baseMode.setSingleHour(Integer.parseInt(baseMode.getHour())-baseMode.getFiveHour()*5);
		   baseMode.setSingleMinute(Integer.parseInt(baseMode.getMinute())-baseMode.getFiveMinute()*5);
		   baseMode.setTopSecondShow(Math.floorMod(Integer.parseInt(baseMode.getSecond()), 2)==0?false:true);
		   
	   }
	   
	   System.out.println(baseMode.toString());
	   
	   //数据校验
	   boolean valid= baseMode.validate();
	   
	   //根据校验结果返回
	   if(valid){
			return baseMode.toDisplay();
		}else{
			return "";
		}
		
	}
	
	//测试
	public static void main(String[] args) {
		TimeConverterImpl tc=new TimeConverterImpl();
		
		String str=tc.convertTime("24:00:00");
		System.out.println(str);
	}
}
