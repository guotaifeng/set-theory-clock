package com.paic.arch.interviews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClockModel extends BaseMode{
	private static final Logger LOG = LoggerFactory.getLogger(ClockModel.class);
	public static int ROW1=4;
	public static int ROW2=4;
	public static int ROW3=11;
	public static int ROW4=4;

	//校验
	public boolean validate(){
		boolean val=true;
		 if(fiveHour>ROW1){
			 val=false;
		 }
		 
		 if(singleHour>ROW2){
			 val=false; 
		 }
		 
		 if(fiveMinute>ROW3){
			 val=false;
		 }
		 
		 if(singleMinute>ROW4){
			 val=false; 
		 }
		 
		 if(!val){
			 LOG.error("The incoming parameter is mistaken;");
		 }
		 return val;
	}
	
	//逻辑处理，返回结果
	public String toDisplay(){
		
		StringBuffer sb=new StringBuffer();
		
		// top display
		if(topSecondShow){
			sb.append("O");
		}else{
			sb.append("Y");
		}
		sb.append("\r\n");
		
		//first row display
		if(fiveHour>=1){
			for(int i=0;i<fiveHour;i++){
				sb.append("R");
			}
		}
		
		for(int j=0;j<ROW1-fiveHour;j++){
			sb.append("O");
		}
		
		sb.append("\r\n");
		
		//second row display
		if(singleHour>=1){
			for(int i=0;i<singleHour;i++){
				sb.append("R");
			}
		}
		
		for(int j=0;j<ROW2-singleHour;j++){
			sb.append("O");
		}
		
		sb.append("\r\n");
		
		//third row display
		if(fiveMinute>=1){
			for(int i=0;i<fiveMinute;i++){
				if((i+1) % 3 == 0){
					sb.append("R");
				}else{
					sb.append("Y");
				}
			}
		}
		
		for(int j=0;j<ROW3-fiveMinute;j++){
			sb.append("O");
		}
		
		sb.append("\r\n");
		
		//four row display
		if(singleMinute>=1){
			for(int i=0;i<singleMinute;i++){
				
					sb.append("Y");
			}
		}
		
		for(int j=0;j<ROW4-singleMinute;j++){
			sb.append("O");
		}
				
		return sb.toString();
	}
}
