package com.paic.arch.interviews;

public abstract class BaseMode {

	protected String time;
	protected String hour;
	protected String minute;
	protected String second;
	protected int fiveHour;
	protected int fiveMinute;
	protected int singleHour;
	protected int singleMinute;
	protected boolean topSecondShow;
	
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public boolean isTopSecondShow() {
		return topSecondShow;
	}
	public void setTopSecondShow(boolean topSecondShow) {
		this.topSecondShow = topSecondShow;
	}
	public String getHour() {
		return hour;
	}
	public void setHour(String hour) {
		this.hour = hour;
	}
	public String getMinute() {
		return minute;
	}
	public void setMinute(String minute) {
		this.minute = minute;
	}
	public String getSecond() {
		return second;
	}
	public void setSecond(String second) {
		this.second = second;
	}
	public int getFiveHour() {
		return fiveHour;
	}
	public void setFiveHour(int fiveHour) {
		this.fiveHour = fiveHour;
	}
	public int getFiveMinute() {
		return fiveMinute;
	}
	public void setFiveMinute(int fiveMinute) {
		this.fiveMinute = fiveMinute;
	}
	public int getSingleHour() {
		return singleHour;
	}
	public void setSingleHour(int singleHour) {
		this.singleHour = singleHour;
	}
	public int getSingleMinute() {
		return singleMinute;
	}
	public void setSingleMinute(int singleMinute) {
		this.singleMinute = singleMinute;
	}
	
	//逻辑处理，返回结果
	public abstract String toDisplay();
	
	//数据校验
	public abstract boolean validate();
	 
	//展示Model数据
	public String toString(){
		
		return "Time: "+this.getTime()+ " Hour: "+this.getHour()+" Minute: "+this.getMinute()+" Second: "+this.getSecond()+" fiveHour: "+this.getFiveHour()+" fiveMinute： "+this.getFiveMinute()+" singleHour: "+this.getSingleHour()+" singleMinute: "+this.getSingleMinute()+" topSecondShow: "+this.isTopSecondShow();
	}
}
